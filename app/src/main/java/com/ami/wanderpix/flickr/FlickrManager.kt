package com.ami.wanderpix.flickr

import com.ami.books.network.FlickrConst
import com.ami.books.network.RetrofitService
import com.ami.wanderpix.model.FlickrPhoto
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers

class FlickrManager {

    private val retrofitService: RetrofitService = RetrofitService()

    fun requestPixForLocation(lat: Double, lon: Double, radius: Double): Observable<Array<FlickrPhoto>> {
        return retrofitService.executeRequest<FlickrPhotosServiceApi>(FlickrConst.BASE_URL)
                .searchPhotosByLocation(lat, lon, radius)
                .subscribeOn(Schedulers.io())
                .map { (photos) -> photos?.photo }
    }
}