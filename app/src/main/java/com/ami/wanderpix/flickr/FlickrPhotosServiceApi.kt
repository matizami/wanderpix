package com.ami.wanderpix.flickr

import com.ami.books.network.FlickrConst
import com.ami.wanderpix.model.FlickrPhotosResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface FlickrPhotosServiceApi {

    @GET(FlickrConst.REST_PATH)
    fun searchPhotosByLocation(@Query("lat") lat: Double,
                               @Query("lon") lon: Double,
                               @Query("radius") radius: Double,
                               @Query("method") method: String = FlickrConst.SEARCH_PHOTOS_METHOD,
                               @Query("api_key") apiKey: String = FlickrConst.API_KEY,
                               @Query("format") format: String = "json",
                               @Query("nojsoncallback") nojsoncallback: Int = 1): Observable<FlickrPhotosResponse>
}