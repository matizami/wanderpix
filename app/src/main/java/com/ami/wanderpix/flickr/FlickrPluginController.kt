package com.ami.wanderpix.flickr

import android.location.Location
import com.ami.wanderpix.event.*
import com.ami.wanderpix.model.FlickrPhoto
import com.csa.climbingtopo.eventbus.RxBus
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.processors.PublishProcessor
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.ReplaySubject

class FlickrPluginController {

    private val disposables: CompositeDisposable = CompositeDisposable()
    private val flickManager: FlickrManager = FlickrManager()
    private var photoStreamSubject: ReplaySubject<FlickrPhoto> = ReplaySubject.create()
    private val photoIds: ArrayList<String> = ArrayList()
    private var locationDisposable: Disposable? = null

    fun startEventListening() {
        disposables.addAll(
                registerPixWalkTriggerEvent(),
                registerFlickrPhotoConnect(),
                registerLocationUpdatesRunning())
    }

    fun stopEventListening() {
        disposables.dispose()
        disposables.clear()
    }

    private fun registerPixWalkTriggerEvent(): Disposable? {
        return RxBus.register(WanderPixDoTriggerEvent::class.java)
                .subscribeBy(
                        onNext = {
                                toggleFlickrWalk(it.start)
                        }
                )
    }

    private fun registerFlickrPhotoConnect(): Disposable? {
        return RxBus.register(FlickrStartPhotoStreamEvent::class.java)
                .subscribeBy ({
                    photoStreamSubject
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeWith(it.observer)
                })
    }

    private fun registerLocationUpdatesRunning(): Disposable? {
        return RxBus.register(LocationUpdatesRunningEvent::class.java)
                .subscribeBy ({
                    if (it.running) {
                        subscribeToLocationUpdates(it.locationUpdatesBus)
                    } else {
                        locationDisposable?.dispose()
                    }
                })
    }

    private fun subscribeToLocationUpdates(locationUpdatesBus: PublishProcessor<Location>) {
        locationDisposable = locationUpdatesBus.subscribeBy(
                onNext = {
                    if (it != null) {
                        performPhotoRequest(it)
                    }
                }
        )
    }

    private fun toggleFlickrWalk(start: Boolean) {
        if (start) {
            RxBus.post(LocationUpdatesDoStartEvent())
        } else {
            RxBus.post(LocationUpdatesDoStopEvent())
        }
    }

    private fun performPhotoRequest(t: Location) {
        flickManager.requestPixForLocation(t.latitude, t.longitude, 0.5)
                .subscribeBy(
                        onNext = {
                            val filteredFlickrPhoto = filterFlickrPhoto(it)
                            filteredFlickrPhoto?.let { photoStreamSubject.onNext(filteredFlickrPhoto) }
                        },
                        onError = {
                            it.printStackTrace()
                        }
                )
    }

    private fun filterFlickrPhoto(photos: Array<FlickrPhoto>): FlickrPhoto? {
        for (photo in photos) {
            if (photoIds.contains(photo.id)) {
                continue
            }
            if (!photo.isPublic()) {
                continue
            }
            photoIds.add(photo.id)
            return photo
        }
        return null
    }
}