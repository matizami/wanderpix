package com.ami.books.network

object FlickrConst {
    const val BASE_URL = "https://api.flickr.com"
    const val REST_PATH: String = "/services/rest/"
    const val API_KEY = "c6b7ba2ee2212e2928107ca12a2403ff"
    const val SECRET_KEY = "c8c1552b0fecf553"
    const val SEARCH_PHOTOS_METHOD = "flickr.photos.search"
}