package com.ami.wanderpix.location

import com.ami.wanderpix.R
import com.ami.wanderpix.WanderPixApplication
import com.ami.wanderpix.event.*
import com.csa.climbingtopo.eventbus.RxBus
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.subscribeBy

class LocationPluginController {

    private lateinit var locationManager: LocationManager
    private val disposables: CompositeDisposable = CompositeDisposable()

    fun startEventListening() {
        disposables.addAll(
                registerActivityLifecycle(),
                registerLocationUpdatesStart(),
                registerLocationUpdatesStop(),
                registerLocationPermissionsGranted(),
                registerLocationSettingsResolutionReceived())
    }

    fun stopEventListening() {
        locationManager.removeLocationUpdates()
        disposables.dispose()
        disposables.clear()
    }

    fun isRequestingLocationUpdates(): Boolean {
        locationManager?.let{ return locationManager.requestingLocationUpdates }
        return false
    }

    private fun registerActivityLifecycle(): Disposable {
        return RxBus.register(LocationActivityLifecycleEvent::class.java)
                .subscribeBy(
                        onNext = {
                            if (it.onCreate) {
                                locationManager = LocationManager.getInstance(it.activity.get()!!)
                            }
                        }
                )
    }

    private fun registerLocationUpdatesStart(): Disposable? {
        return RxBus.register(LocationUpdatesDoStartEvent::class.java)
                .subscribeBy(
                        onNext = {
                            locationManager.startLocationUpdates()
                        }
                )
    }

    private fun registerLocationUpdatesStop(): Disposable? {
        return RxBus.register(LocationUpdatesDoStopEvent::class.java)
                .subscribeBy(
                        onNext = {
                            locationManager.stopLocationUpdates()
                        }
                )
    }

    private fun registerLocationPermissionsGranted(): Disposable {
        return RxBus.register(LocationPermissionsGrantedEvent::class.java)
                .subscribeBy(
                        onNext = {
                            locationManager.onLocationPermissionGranted(it.granted)
                            if (!it.granted) {
                                RxBus.post(ShowErrorMessageEvent(WanderPixApplication.getString(R.string.location_permission_denied_message)))
                            }
                        }
                )
    }

    private fun registerLocationSettingsResolutionReceived(): Disposable? {
        return RxBus.register(LocationSettingsResolutionReceivedEvent::class.java)
                .subscribeBy(
                        onNext = {
                            locationManager.onResolutionReceived(it.accepted)
                        }
                )
    }
}