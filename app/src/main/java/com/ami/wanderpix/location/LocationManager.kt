package com.ami.wanderpix.location

import android.annotation.SuppressLint
import android.app.Activity
import android.location.Location
import android.util.Log
import com.ami.wanderpix.R
import com.ami.wanderpix.WanderPixApplication
import com.ami.wanderpix.event.LocationPermissionsDoCheckEvent
import com.ami.wanderpix.event.LocationSettingsResolutionDoStartEvent
import com.ami.wanderpix.event.LocationUpdatesRunningEvent
import com.ami.wanderpix.event.ShowErrorMessageEvent
import com.csa.climbingtopo.eventbus.RxBus
import com.google.android.gms.location.*
import io.reactivex.processors.PublishProcessor

class LocationManager private constructor(private val activity: Activity) {

    companion object {
        private var locationManager: LocationManager? = null
        fun getInstance(activity: Activity): LocationManager {
            if (null == locationManager) {
                locationManager = LocationManager(activity)
            }
            return locationManager!!
        }
    }

    private val LOCATION_INTERVAL_MILLIS: Long = 41000
    private val REQUEST_FASTEST_INTERVAL_MILLIS: Long = 5000
    private val SMALLEST_DISPLACEMENT_METERS = 100

    private val fusedLocationClient: FusedLocationProviderClient =
            LocationServices.getFusedLocationProviderClient(activity)
    private var locationRequest: LocationRequest = createLocationRequest()
    private var currentBestLocation: Location? = null
    private var locationUpdatesSubject: PublishProcessor<Location> = PublishProcessor.create()
    private var requestingUpdatesPending: Boolean = false
    var requestingLocationUpdates: Boolean = false

    private val locationCallback: LocationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult?) {
            Log.d("New location:", locationResult?.lastLocation?.accuracy.toString())
            locationResult?.lastLocation?.let {
                if (isBetterLocation(locationResult.lastLocation)) {
                    notifyLocationUpdate(locationResult.lastLocation)
                }
            }
        }
    }

    private fun notifyLocationUpdate(location: Location) {
        if (location == currentBestLocation) return
        locationUpdatesSubject.onNext(location)
        currentBestLocation = location
    }

    fun startLocationUpdates() {
        if (!requestingLocationUpdates && !requestingUpdatesPending) {
            requestingUpdatesPending = true
            verifyLocationPermissions()
        }
    }

    fun stopLocationUpdates() {
        removeFusedClientLocationUpdates()
    }

    fun removeLocationUpdates() {
        removeFusedClientLocationUpdates()
        locationUpdatesSubject.onComplete()
    }

    fun onLocationPermissionGranted(granted: Boolean) {
        if (granted)  {
            verifyLocationSettings()
        } else {
            requestingUpdatesPending = false
        }
    }

    fun onResolutionReceived(accepted: Boolean) {
        if (accepted) {
            requestFusedClientLocationUpdates()
            return
        }
        this.onLocationSettingsUnsatisfied()
    }

    private fun createLocationRequest(): LocationRequest {
        val locationRequest = LocationRequest()
        locationRequest.priority = LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY
        locationRequest.interval = LOCATION_INTERVAL_MILLIS
        locationRequest.fastestInterval = REQUEST_FASTEST_INTERVAL_MILLIS
        locationRequest.smallestDisplacement = SMALLEST_DISPLACEMENT_METERS.toFloat()
        return locationRequest
    }

    @SuppressLint("MissingPermission")
    private fun requestFusedClientLocationUpdates() {
        requestingUpdatesPending = false
        requestingLocationUpdates = true
        fusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null)
        RxBus.post(LocationUpdatesRunningEvent(true, locationUpdatesSubject))
    }

    private fun removeFusedClientLocationUpdates() {
        requestingLocationUpdates = false
        fusedLocationClient.removeLocationUpdates(locationCallback)
        RxBus.post(LocationUpdatesRunningEvent(false, locationUpdatesSubject))
    }

    private fun verifyLocationPermissions() {
        if (null == activity) {
            requestingUpdatesPending = false
            return
        }
        RxBus.post(LocationPermissionsDoCheckEvent())
    }

    private fun verifyLocationSettings() {
        if (null == activity) {
            requestingUpdatesPending = false
            return
        }
        RxBus.post(LocationSettingsResolutionDoStartEvent(getLocationSettingsRequestBuilder().build()))
    }

    private fun getLocationSettingsRequestBuilder(): LocationSettingsRequest.Builder {
        val builder: LocationSettingsRequest.Builder =
                LocationSettingsRequest.Builder()
                        .addLocationRequest(locationRequest)
                        .setNeedBle(true)
        return builder
    }

    private fun onLocationSettingsUnsatisfied() {
        requestingUpdatesPending = false
        RxBus.post(ShowErrorMessageEvent(
                WanderPixApplication.getString(R.string.location_settings_unsatisfied_message)))
    }

    private fun isBetterLocation(location: Location): Boolean {
        if (currentBestLocation == null) {
            return true
        }

        val TWO_MINUTES = 1000 * 60 * 2
        val timeDelta = location.time - currentBestLocation!!.time
        val isSignificantlyNewer = timeDelta > TWO_MINUTES
        val isSignificantlyOlder = timeDelta < -TWO_MINUTES
        val isNewer = timeDelta > 0
        if (isSignificantlyNewer) {
            return true
        } else if (isSignificantlyOlder) {
            return false
        }

        val accuracyDelta = (location.accuracy - currentBestLocation!!.accuracy).toInt()
        val isLessAccurate = accuracyDelta > 0
        val isMoreAccurate = accuracyDelta < 0
        val isSignificantlyLessAccurate = accuracyDelta > 200

        val isFromSameProvider = isSameProvider(location.provider,
                currentBestLocation!!.provider)

        if (isMoreAccurate) {
            return true
        } else if (isNewer && !isLessAccurate) {
            return true
        } else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
            return true
        }
        return false
    }

    private fun isSameProvider(provider1: String?, provider2: String?): Boolean {
        if (provider1 == null) {
            return provider2 == null
        }
        return provider1 == provider2
    }
}