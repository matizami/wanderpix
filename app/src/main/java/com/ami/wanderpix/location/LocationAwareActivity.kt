package com.ami.wanderpix.location

import android.Manifest
import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.content.IntentSender
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import com.ami.wanderpix.R
import com.ami.wanderpix.event.*
import com.csa.climbingtopo.eventbus.RxBus
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.CommonStatusCodes
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.LocationSettingsRequest
import com.google.android.gms.location.LocationSettingsStatusCodes
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy
import java.lang.ref.WeakReference

open class LocationAwareActivity : AppCompatActivity() {

    private val REQUEST_CHECK_SETTINGS: Int = 100
    private val PERMISSIONS_REQUEST_FINE_LOCATION: Int = 101

    private var disposables: CompositeDisposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        RxBus.post(LocationActivityLifecycleEvent.onCreate(WeakReference<Activity>(this)))
        registerSettingsResolution()
        registerLocationPermission()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode === REQUEST_CHECK_SETTINGS) {
            notifyResolutionReceived(resultCode == Activity.RESULT_OK)
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            PERMISSIONS_REQUEST_FINE_LOCATION -> notifyPermissionsGranted(permissionGranted(grantResults))
            else -> super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    override fun onDestroy() {
        disposables.dispose()
        RxBus.post(LocationActivityLifecycleEvent.onDestroy(WeakReference<Activity>(this)))
        super.onDestroy()
    }

    private fun registerSettingsResolution() {
        disposables.add(RxBus.register(LocationSettingsResolutionDoStartEvent::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(
                    onNext = {
                        verifySettingsResolution(it.settingsRequest)
                    }
                )
        )
    }

    private fun verifySettingsResolution(settingsRequest: LocationSettingsRequest) {
        LocationServices.getSettingsClient(this)
                .checkLocationSettings(settingsRequest)
                .addOnSuccessListener({
                    tResult -> notifyResolutionReceived(true)
                })
                .addOnFailureListener({
                    exception -> handleSettingsResolutionFailure(exception as ApiException)
                })
    }

    private fun handleSettingsResolutionFailure(exception: ApiException) {
        var statusCode: Int = (exception).statusCode
        when (statusCode) {
            CommonStatusCodes.RESOLUTION_REQUIRED ->
                showSettingsResolutionDialog(exception as ResolvableApiException)
            LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE ->
                notifyResolutionReceived(false)
        }
    }

    private fun showSettingsResolutionDialog(exception: ResolvableApiException) {
        try {
            exception.startResolutionForResult(this, REQUEST_CHECK_SETTINGS);
        } catch (sendException: IntentSender.SendIntentException) {
            notifyResolutionReceived(false)
        }
    }

    private fun notifyResolutionReceived(resolutionOk: Boolean) {
        RxBus.post(LocationSettingsResolutionReceivedEvent(resolutionOk))
    }

    private fun registerLocationPermission() {
        disposables.add(RxBus.register(LocationPermissionsDoCheckEvent::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(
                        onNext = {
                            verifyLocationPermission()
                        }
                )
        )
    }

    private fun verifyLocationPermission() {
        if (shouldRequestLocationPermission()) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                showPermissionExplanation()
            } else {
                ActivityCompat.requestPermissions(this,
                        arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                        PERMISSIONS_REQUEST_FINE_LOCATION)
            }
            return
        }
        notifyPermissionsGranted(true)
    }

    private fun shouldRequestLocationPermission() = (
            ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED
            )

    private fun showPermissionExplanation() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle(getString(R.string.permission_explanation_title))
                .setMessage(getString(R.string.permission_explanation_message))
                .setPositiveButton(android.R.string.ok, DialogInterface.OnClickListener {
                    dialog,
                    id ->
                    ActivityCompat.requestPermissions(this,
                            arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                            PERMISSIONS_REQUEST_FINE_LOCATION)
                })
        builder.create().show()
    }

    private fun permissionGranted(grantResults: IntArray) = (
            grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)

    private fun notifyPermissionsGranted(granted: Boolean) {
        RxBus.post(LocationPermissionsGrantedEvent(granted))
    }
}