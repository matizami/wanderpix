package com.ami.wanderpix.ui

import com.ami.wanderpix.event.LocationUpdatesRunningEvent
import com.ami.wanderpix.event.WanderPixDoTriggerEvent
import com.ami.wanderpix.event.ShowErrorMessageEvent
import com.csa.climbingtopo.eventbus.RxBus
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy

class WanderPixPresenter {

    private var view: WanderPixView? = null
    private var disposables: CompositeDisposable = CompositeDisposable()
    var running: Boolean = false

    fun start(view: WanderPixView, running: Boolean) {
        this.view = view
        this.running = running
        registerLocationUpdatesRunning()
        registerErrors()
    }

    private fun registerLocationUpdatesRunning() {
        disposables.add(RxBus.register(LocationUpdatesRunningEvent::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy({
                    this.running = it.running
                    view?.updateToolbar()
                })
        )
    }

    private fun registerErrors() {
        disposables.add(RxBus.register(ShowErrorMessageEvent::class.java)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy({
                    view?.showError(it.message)
                })
        )
    }

    fun toggleLocation(): Boolean {
        RxBus.post(WanderPixDoTriggerEvent(!running))
        return true
    }

    fun stop() {
        this.view = null
        disposables.dispose()
    }
}