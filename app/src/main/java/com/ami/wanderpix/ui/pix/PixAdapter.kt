package com.ami.wanderpix.ui.pix

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.ami.wanderpix.R
import com.ami.wanderpix.model.FlickrPhoto
import com.squareup.picasso.Picasso

class PixAdapter(var context: Context) : RecyclerView.Adapter<PixAdapter.PixViewHolder>() {

    var photos: ArrayList<FlickrPhoto> = ArrayList()

    override fun onBindViewHolder(viewHolder: PixViewHolder?, position: Int) {
        val pixItem = photos[position]
        Picasso.with(context).load(pixItem.getPhotoUrl()).into(viewHolder?.pixImageView)
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): PixViewHolder {
        val view = LayoutInflater.from(parent?.context).inflate(viewType, parent, false)
        return PixViewHolder(view)
    }

    override fun getItemCount(): Int {
        return photos.count()
    }

    override fun getItemViewType(position: Int): Int {
        return R.layout.pix_item
    }

    fun setItems(photos: ArrayList<FlickrPhoto>): Unit {
        this.photos = photos
        notifyDataSetChanged()
    }

    fun addItem(photo: FlickrPhoto): Unit {
        photos.add(0, photo)
        notifyItemInserted(0)
    }

    class PixViewHolder : RecyclerView.ViewHolder {
        var pixImageView: ImageView

        constructor(itemView: View) : super(itemView) {
            pixImageView = itemView.findViewById<ImageView>(R.id.pixImageView)
        }
    }

    fun clearItems() {
        photos.clear()
        notifyDataSetChanged()
    }
}
