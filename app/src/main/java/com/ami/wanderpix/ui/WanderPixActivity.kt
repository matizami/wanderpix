package com.ami.wanderpix.ui

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.view.Menu
import android.view.MenuItem
import com.ami.wanderpix.R
import com.ami.wanderpix.WanderPixApplication
import com.ami.wanderpix.location.LocationAwareActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_pix.*

class WanderPixActivity : LocationAwareActivity(), WanderPixView {

    private lateinit var presenter: WanderPixPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        presenter = WanderPixPresenter()
        presenter.start(this, isRunning())
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        var title: String = getString(R.string.action_start)
        if (isRunning()) {
            title = getString(R.string.action_stop)
        }
        menu!!.findItem(R.id.action_toggle_location)!!.title = title
        return super.onPrepareOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_toggle_location -> presenter.toggleLocation()
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun updateToolbar() {
        invalidateOptionsMenu()
    }

    override fun showError(error: String) {
        Snackbar.make(recyclerView, error, Snackbar.LENGTH_LONG).show()
    }

    override fun onDestroy() {
        presenter.stop()
        super.onDestroy()
    }

    private fun isRunning() = (application as WanderPixApplication).isRequestingLocationUpdates()
}
