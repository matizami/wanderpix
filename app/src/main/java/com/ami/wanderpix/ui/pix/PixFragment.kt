package com.ami.wanderpix.ui.pix

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ami.wanderpix.R
import com.ami.wanderpix.model.FlickrPhoto
import kotlinx.android.synthetic.main.fragment_pix.*

class PixFragment : Fragment(), PixView {

    private lateinit var adapter: PixAdapter
    private lateinit var layoutManager: RecyclerView.LayoutManager
    private val presenter: PixPresenter by lazy { PixPresenter() }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_pix, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerView()
        presenter.start(this)
    }

    private fun initRecyclerView() {
        layoutManager = LinearLayoutManager(context)
        adapter = PixAdapter(context)
        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = adapter
    }

    override fun showPhoto(photo: FlickrPhoto) {
        adapter.addItem(photo)
    }

    override fun clearPhotos() {
        adapter.clearItems()
    }

    override fun onDestroyView() {
        presenter.stop()
        super.onDestroyView()
    }
}