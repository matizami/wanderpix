package com.ami.wanderpix.ui


interface WanderPixView {
    fun showError(error: String): Unit
    fun updateToolbar()
}