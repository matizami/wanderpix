package com.ami.wanderpix.ui.pix

import android.widget.Toast
import com.ami.wanderpix.WanderPixApplication
import com.ami.wanderpix.event.FlickrStartPhotoStreamEvent
import com.ami.wanderpix.model.FlickrPhoto
import com.csa.climbingtopo.eventbus.RxBus
import io.reactivex.observers.DisposableObserver


class PixPresenter {

    private var view: PixView? = null
    private var observer: DisposableObserver<FlickrPhoto> = object : DisposableObserver<FlickrPhoto>() {

        override fun onComplete() {
            Toast.makeText(WanderPixApplication.instance, "Pix Walk complete!", Toast.LENGTH_SHORT).show()
        }

        override fun onNext(t: FlickrPhoto) {
            view?.showPhoto(t)
        }

        override fun onError(e: Throwable?) {
            e?.printStackTrace()
        }
    }

    fun start(view: PixView) {
        this.view = view
        RxBus.post(FlickrStartPhotoStreamEvent(observer))
    }

    fun stop() {
        observer.dispose()
        this.view = null
    }
}