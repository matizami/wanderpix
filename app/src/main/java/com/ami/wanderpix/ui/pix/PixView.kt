package com.ami.wanderpix.ui.pix

import com.ami.wanderpix.model.FlickrPhoto


interface PixView {
    fun showPhoto(photo: FlickrPhoto): Unit
    fun clearPhotos(): Unit
}