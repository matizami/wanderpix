package com.ami.wanderpix

import android.app.Application
import android.support.annotation.StringRes
import com.ami.wanderpix.flickr.FlickrPluginController
import com.ami.wanderpix.location.LocationPluginController


class WanderPixApplication : Application() {

    companion object {
        lateinit var instance: WanderPixApplication
        fun getString(@StringRes resId: Int): String {
            return instance.getString(resId)
        }
    }

    private var locationController: LocationPluginController = LocationPluginController()
    private var flickrController: FlickrPluginController = FlickrPluginController()

    override fun onCreate() {
        super.onCreate()
        instance = this
        locationController.startEventListening()
        flickrController.startEventListening()
    }

    override fun onTerminate() {
        locationController.stopEventListening()
        flickrController.stopEventListening()
        super.onTerminate()
    }

    fun isRequestingLocationUpdates(): Boolean {
        return locationController.isRequestingLocationUpdates()
    }
}