package com.ami.wanderpix.event

import com.google.android.gms.location.LocationSettingsRequest


data class LocationSettingsResolutionDoStartEvent(
        var settingsRequest: LocationSettingsRequest
)