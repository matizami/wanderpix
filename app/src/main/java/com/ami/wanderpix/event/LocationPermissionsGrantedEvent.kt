package com.ami.wanderpix.event

data class LocationPermissionsGrantedEvent(
        var granted: Boolean
)