package com.ami.wanderpix.event

import android.app.Activity
import java.lang.ref.WeakReference


class LocationActivityLifecycleEvent(var activity: WeakReference<out Activity>) {
    var onCreate = false
    var onStart = false
    var onStop = false
    var onDestroy = false

    companion object {

        fun onCreate(activity: WeakReference<out Activity>): LocationActivityLifecycleEvent {
            val event = LocationActivityLifecycleEvent(activity)
            event.onCreate = true
            return event
        }

        fun onStart(activity: WeakReference<out Activity>): LocationActivityLifecycleEvent {
            val event = LocationActivityLifecycleEvent(activity)
            event.onStart = true
            return event
        }

        fun onStop(activity: WeakReference<out Activity>): LocationActivityLifecycleEvent {
            val event = LocationActivityLifecycleEvent(activity)
            event.onStop = true
            return event
        }

        fun onDestroy(activity: WeakReference<out Activity>): LocationActivityLifecycleEvent {
            val event = LocationActivityLifecycleEvent(activity)
            event.onDestroy = true
            return event
        }
    }
}