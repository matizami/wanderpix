package com.ami.wanderpix.event


data class ShowErrorMessageEvent(
        var message: String
)