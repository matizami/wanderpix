package com.ami.wanderpix.event

import android.location.Location
import io.reactivex.processors.PublishProcessor

data class LocationUpdatesRunningEvent(
        var running: Boolean,
        var locationUpdatesBus: PublishProcessor<Location>
)