package com.ami.wanderpix.event


data class LocationSettingsResolutionReceivedEvent(
        var accepted: Boolean
)