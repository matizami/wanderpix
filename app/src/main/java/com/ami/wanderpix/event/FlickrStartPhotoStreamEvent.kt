package com.ami.wanderpix.event

import com.ami.wanderpix.model.FlickrPhoto
import io.reactivex.Observer


data class FlickrStartPhotoStreamEvent(
        var observer: Observer<FlickrPhoto>
)