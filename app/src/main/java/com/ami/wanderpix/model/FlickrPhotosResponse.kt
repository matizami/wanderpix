package com.ami.wanderpix.model

data class FlickrPhotosResponse(
        var photos: FlickrPhotoCollection,
        var stat: String
)