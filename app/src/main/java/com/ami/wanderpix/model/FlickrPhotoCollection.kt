package com.ami.wanderpix.model


data class FlickrPhotoCollection(
        var page: Int,
        var pages: Int,
        var total: String,
        var photo: Array<FlickrPhoto>
)