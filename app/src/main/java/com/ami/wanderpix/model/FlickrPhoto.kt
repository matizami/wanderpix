package com.ami.wanderpix.model

data class FlickrPhoto (
        var id: String,
        var owner: String,
        var secret: String,
        var server: String,
        var farm: Int,
        var title: String,
        var ispublic: Int,
        var isfriend: Int,
        var isfamily: Int) {

    fun getPhotoUrl(): String {
        return "https://farm" + farm + ".staticflickr.com/"+ server +"/"+ id +"_"+ secret +"_c.jpg"
    }

    fun isPublic(): Boolean {
        return ispublic == 1
    }
}