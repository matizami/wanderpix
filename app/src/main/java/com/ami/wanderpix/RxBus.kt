package com.csa.climbingtopo.eventbus

import android.util.Log
import com.jakewharton.rxrelay2.PublishRelay
import io.reactivex.Observable

class RxBus private constructor() {

    private val busSubject = PublishRelay.create<Any>().toSerialized()

    fun <T> register(eventClass: Class<T>): Observable<T> {
        return busSubject
                .filter { event -> event.javaClass == eventClass }
                .map { obj -> obj as T }
    }

    fun post(event: Any): Unit {
        Log.d("RxBux posted event: ", event.javaClass.simpleName)
        busSubject.accept(event)
    }

    companion object {
        @JvmStatic var rxBus: RxBus? = null
        fun getInstance(): RxBus {
            if (null == rxBus) {
                rxBus = RxBus()
            }
            return rxBus!!
        }
        fun post(event: Any): Unit {
            return getInstance().post(event)
        }
        fun <T> register(eventClass: Class<T>): Observable<T> {
            return getInstance().register(eventClass)
        }
    }
}
